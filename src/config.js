export default new Promise(function(resolve, reject) {
  const configUrl = window.location.origin +
    window.location.pathname.replace(/[^\/]*$/, 'config.json');

  var request = new XMLHttpRequest();
  request.addEventListener("load", function() {
    resolve(JSON.parse(request.responseText));
  });
  request.open("GET", configUrl);
  request.send();
});
