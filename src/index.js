"use strict";
// Copyright 2019 fingercomp
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.


import videojs from "video.js";
import "video.js/dist/video-js.min.css";

import "videojs-contrib-media-sources";
import "videojs-contrib-hls.js";

import "./index.css";

import config from './config.js';

function initializePlayer(id) {
  var playerEl = document.getElementById(id);
  playerEl.classList.add("video-js");

  var player = videojs(playerEl, {
    controls: true,
    autoplay: false,
    preload: "auto"
  });
}

function updateFromConfig(config) {
  document.getElementById("streamName").innerText = config.streamName;
  
  var source = document.createElement("source");
  source.src = config.streamUrl;
  source.type = "application/x-mpegURL";
  document.getElementById("player")
    .appendChild(source);

  var iframe = document.createElement("iframe");
  iframe.sandbox = "allow-same-origin allow-scripts allow-forms"
  iframe.src = "https://webchat.esper.net/?nick=aqueduct...&channels=" +
    config.channels;
  document.getElementById("chat")
    .appendChild(iframe);

  document.getElementById("container").style.display = "";
  initializePlayer("player");
}

config.then(updateFromConfig);
